package com.example.lab7gui;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import Guest.Guest;
import Restaurant.Restaurant;
import Guest.SitInGuest;
import Guest.OrderFoodGuest;
import Waiter.Waiter;
import javafx.util.Duration;
import java.util.*;

public class HelloApplication extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        Label labelWaiters = new Label("Number of Waiters:");
        Label labelSitInGuests = new Label("Number of SitIn Guests:");
        Label labelOrderFoodGuests = new Label("Number of OrderFood Guests:");
        Label labelTables = new Label("Number of Tables:");

        TextField textFieldWaiters = new TextField(Integer.toString(2));
        TextField textFieldSitInGuests = new TextField(Integer.toString(5));
        TextField textFieldOrderFoodGuests = new TextField(Integer.toString(7));
        TextField textFieldTables = new TextField(Integer.toString(10));
        primaryStage.setTitle("Restaurant Simulation");

        Stage guestInfoStage = new Stage();
        guestInfoStage.setTitle("Guest Info");

        Button startSimulationButton = new Button("Start Simulation");

        startSimulationButton.setOnAction(event -> {
            startSimulation(
                    Integer.parseInt(textFieldWaiters.getText()),
                    Integer.parseInt(textFieldSitInGuests.getText()),
                    Integer.parseInt(textFieldOrderFoodGuests.getText()),
                    Integer.parseInt(textFieldTables.getText()),
                    guestInfoStage
            );
        });

        VBox vbox = new VBox(
                new HBox(labelWaiters, textFieldWaiters),
                new HBox(labelSitInGuests, textFieldSitInGuests),
                new HBox(labelOrderFoodGuests, textFieldOrderFoodGuests),
                new HBox(labelTables, textFieldTables),
                startSimulationButton
        );
        vbox.setSpacing(10);
        vbox.setPadding(new Insets(20));

        Scene scene = new Scene(vbox, 400, 200);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void startSimulation(int numWaiters, int numSitInGuests,
                                 int numOrderFoodGuests, int numTables, Stage guestInfoStage) {
        Map<Thread, String> lastStateChangeMap = new HashMap<>();

        Restaurant restaurant = new Restaurant(numTables, numWaiters);
        List<Waiter> waiters = createWaiters(numWaiters);
        List<Thread> guests = createAndRunGuests(numSitInGuests, numOrderFoodGuests, restaurant, waiters);
        synchronizeAndNotify(waiters);
        synchronizeAndNotify(restaurant);

        Timeline timeline = new Timeline(
                new KeyFrame(Duration.seconds(1), event ->
                        updateGuestInfo(guests, lastStateChangeMap, guestInfoStage))
        );
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

    private void updateGuestInfo(List<Thread> guests, Map<Thread, String> lastStateMap, Stage guestInfoStage) {
        Platform.runLater(() -> {
            VBox guestInfoPanel = new VBox();

            guests.forEach(guestThread -> {
                String currentState = getGuestInfo(guestThread, lastStateMap);
                lastStateMap.put(guestThread, currentState);

                Label guestLabel = new Label(currentState);
                setTextColor(guestLabel, guestThread.getState());

                Button suspendButton = new Button("Suspend");
                suspendButton.setOnAction(event -> suspendGuest(guestThread));

                Button resumeButton = new Button("Resume");
                resumeButton.setOnAction(event -> resumeGuest(guestThread));

                HBox buttonBox = new HBox(suspendButton, resumeButton);
                buttonBox.setSpacing(10);

                VBox guestInfoBox = new VBox(guestLabel, buttonBox);
                VBox.setMargin(buttonBox, new Insets(0, 0, 0, 20));

                guestInfoPanel.getChildren().add(guestInfoBox);
            });

            ScrollPane scrollPane = new ScrollPane();
            scrollPane.setContent(guestInfoPanel);

            Scene guestInfoScene = new Scene(scrollPane, 800, 600);
            guestInfoStage.setScene(guestInfoScene);
            guestInfoStage.show();
        });
    }

    private void suspendGuest(Thread guestThread) {
        guestThread.suspend();
        System.out.println("Thread " + guestThread.getName() + " suspended.");
    }

    private void resumeGuest(Thread guestThread) {
        guestThread.resume();
        System.out.println("Thread " + guestThread.getName() + " resumed.");
    }

    private String getGuestInfo(Thread guestThread, Map<Thread, String> lastStateMap) {
        String currentState = guestThread.getState().toString();
        String lastState = lastStateMap.getOrDefault(guestThread, "");

        if (!currentState.equals(lastState)) {
            lastStateMap.put(guestThread, currentState);
            return String.format("%s: State - %s, Priority - %d",
                    guestThread.getName(), currentState, guestThread.getPriority());
        }

        return String.format("%s: State - %s, Priority - %d, Timestamp - %s",
                guestThread.getName(), currentState, guestThread.getPriority(), lastStateMap.get(guestThread));
    }

    private void setTextColor(Label label, Thread.State state) {
        switch (state) {
            case NEW:
                label.setStyle("-fx-text-fill: blue");
                break;
            case RUNNABLE:
                label.setStyle("-fx-text-fill: green");
                break;
            case BLOCKED:
                label.setStyle("-fx-text-fill: orange");
                break;
            case WAITING:
                label.setStyle("-fx-text-fill: purple");
                break;
            case TIMED_WAITING:
                label.setStyle("-fx-text-fill: brown");
                break;
            case TERMINATED:
                label.setStyle("-fx-text-fill: red");
                break;
        }
    }

    private static List<Waiter> createWaiters(int numWaiters) {
        List<Waiter> waiters = new ArrayList<>();
        for (int i = 0; i < numWaiters; i++) {
            waiters.add(new Waiter());
        }
        return waiters;
    }

    private static List<Thread> createAndRunGuests(int numSitInGuests, int numOrderFoodGuests, Restaurant restaurant, List<Waiter> waiters) {
        List<Thread> guests = new ArrayList<>();
        int totalGuests = numSitInGuests + numOrderFoodGuests;

        for (int i = 0; i < totalGuests; i++) {
            Guest guest;
            if (i < numSitInGuests) {
                guest = new SitInGuest("SitInGuest" + i, restaurant, getWaiter(waiters, i));
            } else {
                synchronizeAndNotify(waiters);
                guest = new OrderFoodGuest("OrderFoodGuest" + (i - numSitInGuests), restaurant, getWaiter(waiters, i));
            }
            Thread guestThread = createAndConfigureThread(guest, guest.getName());
            guests.add(guestThread);
        }
        guests.forEach(Thread::start);
        return guests;
    }

    private static Waiter getWaiter(List<Waiter> waiters, int guestIndex) {
        if (waiters.isEmpty()) {
            return null;
        }
        return waiters.get(guestIndex % waiters.size());
    }

    private static Thread createAndConfigureThread(Guest guest, String name) {
        Thread guestThread = new Thread(guest);
        configureThread(guestThread, name);
        return guestThread;
    }

    private static void configureThread(Thread thread, String name) {
        Random random = new Random();
        int priority = Thread.MIN_PRIORITY + random.nextInt(Thread.MAX_PRIORITY - Thread.MIN_PRIORITY + 1);

        thread.setPriority(priority);
        thread.setName(name);
    }

    private static void synchronizeAndNotify(Restaurant restaurant) {
        synchronized (restaurant) {
            restaurant.notifyAll();
        }
    }

    private static void synchronizeAndNotify(List<Waiter> waiters) {
        synchronized (waiters) {
            waiters.notifyAll();
        }
    }

    private static void suspendAndResumeGuest(List<Thread> guests) {
        try {
            Thread.sleep(3000);
            Thread firstGuest = guests.get(0);
            firstGuest.suspend();
            System.out.println("Thread " + firstGuest.getName() + " suspended.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
