package Guest;

public interface Guest extends Runnable {
    String getName();
}
