package Guest;
import Restaurant.Restaurant;
import Waiter.Waiter;

public class OrderFoodGuest implements Guest {
    private String name;
    private Restaurant restaurant;
    private Waiter waiter;

    public OrderFoodGuest(String name, Restaurant restaurant, Waiter waiter) {
        this.name = name;
        this.restaurant = restaurant;
        this.waiter = waiter;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void run() {
        System.out.println(name + " arrived at the restaurant.");

        //synchronized (restaurant) {
            if (restaurant.orderFood(this)) {
                System.out.println(name + " is waiting for the waiter to take the order.");
                if (waiter == null) return;
                waiter.prepareFood(this);
            } else {
                try {
                    restaurant.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Thread.currentThread().interrupt();
                }
            }
        //}
    }


}
