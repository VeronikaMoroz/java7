package Guest;

import Restaurant.Restaurant;
import Waiter.Waiter;

public class SitInGuest implements Guest {
    private String name;
    private Restaurant restaurant;
    private Waiter waiter;

    public SitInGuest(String name, Restaurant restaurant, Waiter waiter) {
        this.name = name;
        this.restaurant = restaurant;
        this.waiter = waiter;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void run() {
        System.out.println(name + " arrived at the restaurant.");
        if (restaurant.reserveTable(this)) {
            System.out.println(name + " is sitting at the table.");
            if (waiter == null) {
                // If waiter is null, put the thread into a waiting state
                synchronized (this) {
                    try {
                        System.out.println(name + " is waiting for a waiter.");
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                waiter.serveTable(this);
            }
        }
    }
}
