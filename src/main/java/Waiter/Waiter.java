package Waiter;
import Guest.Guest;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class Waiter {
    private Lock serviceLock;

    public Waiter() {
        this.serviceLock = new ReentrantLock();
    }

    public void serveTable(Guest guest) {
        serviceLock.lock();
        try {
            System.out.println("Waiter is serving table for " + guest.getName());
            Thread.sleep(2000);
            System.out.println("Waiter finished serving table for " + guest.getName());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            serviceLock.unlock();
        }
    }

    public void prepareFood(Guest guest) {
        serviceLock.lock();
        try {
            System.out.println("Waiter is preparing food for " + guest.getName());
            Thread.sleep(2000);
            System.out.println("Waiter finished preparing food for " + guest.getName());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            serviceLock.unlock();
        }
    }
}
