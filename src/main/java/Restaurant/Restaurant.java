package Restaurant;

import Guest.Guest;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class Restaurant {
    private int availableTables;
    private int availableWaiters;
    private Lock tableLock;
    private Lock waiterLock;

    public Restaurant(int numTables, int numWaiters) {
        this.availableTables = numTables;
        this.availableWaiters = numWaiters;
        this.tableLock = new ReentrantLock();
        this.waiterLock = new ReentrantLock();
    }

    public boolean reserveTable(Guest guest) {
        tableLock.lock();
        try {
            if (availableTables > 0) {
                System.out.println(guest.getName() + " reserved a table.");
                availableTables--;
                return true;
            } else {
                System.out.println(guest.getName() + " couldn't reserve a table. No tables available.");
                return false;
            }
        } finally {
            availableTables++;
            tableLock.unlock();
        }
    }

    public boolean orderFood(Guest guest) {
        waiterLock.lock();
        try {
            if (availableWaiters > 0) {
                System.out.println(guest.getName() + " ordered food with the waiter.");
                availableWaiters--;
                return true;
            } else {
                System.out.println(guest.getName() + " couldn't order food. No waiters available.");
                return false;
            }
        } finally {
            availableWaiters++;
            waiterLock.unlock();
        }
    }
}
