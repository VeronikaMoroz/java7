package Restaurant;

public class Table {
    private boolean occupied;

    public Table() {
        this.occupied = false;
    }

    public boolean isOccupied() {
        return occupied;
    }

    public void setOccupied(boolean occupied) {
        this.occupied = occupied;
    }
}
